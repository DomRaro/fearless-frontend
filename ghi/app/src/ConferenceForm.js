import React, { useEffect, useState } from 'react';

function ConferenceForm() {

    const handleSubmit = async (event) => {
        console.log("handle submit activated")
        event.preventDefault();
        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = max_attendees;
        data.location = location;
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [start, setStart] = useState('');

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const [end, setEnd] = useState('');

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const [description, setDescription] = useState('');

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [presentations, setPresentations] = useState('');

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setPresentations(value);
    }

    const [max_attendees, setMaxAttendees] = useState('');

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            console.log('*************', data)
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStartChange} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control" />
                                <label htmlFor="start_date">Start date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEndChange} placeholder="End date" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="end_date">End date</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">Conference description</label>
                                <textarea onChange={handleDescriptionChange} placeholder="Conference description" className="form-control" name="description" id="description" rows="8"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxPresentationsChange} placeholder="Max presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">Max presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxAttendeesChange} placeholder="Max attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">Max attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ConferenceForm