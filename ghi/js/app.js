function createCard(name, description, pictureUrl, starts, ends, locationName) {
  return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded grid gap-0 column-gap-3 ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        Starts: ${starts}, Ends: ${ends}
      </div>
      </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("response is not okay just like you")
      return `<div class="alert alert-primary" role="alert">
            Something went horribly wrong. Self destructing in ten seconds.
          </div>`
    } else {
      const data = await response.json();
      let i = 0
      for (let conference of data.conferences) {
        i++;
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const locationName = details.conference.location.name;
          const starts = new Date(details.conference.starts).toDateString();
          const ends = new Date(details.conference.ends).toDateString();
          const html = createCard(title, description, pictureUrl, starts, ends, locationName);
          const column = document.querySelector(`#col-${i}`);
          column.innerHTML += html;
          console.log(details)
        }
        if (i == 3) {
          i = 0
        }

      }

    }
  } catch (e) {
    console.error(e)
  }

});

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//         const response = await fetch(url);
//         if (!response.ok) {
//             console.error("response is not okay just like you")
//         } else {
//             const data = await response.json()

//             for (let conference of data.conferences) {
//                 const conference = data.conferences[0];
//                 const nameTag = document.querySelector('.card-title')
//                 // nameTag.innerHTML = conference.name

//                 const detailUrl = `http://localhost:8000${conference.href}`;
//                 const detailResponse = await fetch(detailUrl);
//                 if (detailResponse.ok) {
//                     const details = await detailResponse.json()
//                     const conferenceDetail = details.conference.description
//                     const confDetail = document.querySelector('.card-text')
//                     // confDetail.innerHTML = conferenceDetail
//                     // const imageTag = document.querySelector('.card-img-top')
//                     // imageTag.src = details.conference.location.picture_url;
//                     const html = createCard(name, description, pictureUrl);
//                     const column = document.querySelector('.col');
//                     column.innerHTML += html;

//                 }
//             }




//         }
//     } catch (e) {
//         console.error(e)
//     }

// });
