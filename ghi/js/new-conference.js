function createOption(name, id) {
    return `<option value=${id}>${name}</option>`
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(url)

        if (!response.ok) {
            console.error("response is givin you the finger my guy");
            return `<div class="alert alert-primary" role="alert">
            Something went horribly wrong. Self destructing in ten seconds.
          </div>`;
        } else {

            const data = await response.json();
            console.log(data)
            const locationOption = document.querySelector(`#location`);
            for (let location of data.locations) {
                const name = location.name
                const id = location.id
                const html = createOption(name, id)
                locationOption.innerHTML += html


                // const option = document.createElement('option');
                // option.innerHTML = location.name;
                // option.value = location.href;
                // locationOption.appendChild(option);
                // console.log(state)

            }
        }
    } catch (e) {
        console.error(e)
    }
})

window.addEventListener('DOMContentLoaded', async () => {

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});