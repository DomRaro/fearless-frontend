function createOption(name, abbreviation) {
    return `<option value=${abbreviation}>${name}</option>`
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url)

        if (!response.ok) {
            console.error("response is givin you the finger my guy");
            return `<div class="alert alert-primary" role="alert">
            Something went horribly wrong. Self destructing in ten seconds.
          </div>`;
        } else {

            const data = await response.json();
            console.log(data)
            const stateID = document.querySelector(`#state`);
            for (let state of data.states) {
                const name = Object.keys(state);
                const abbreviation = Object.values(state)
                const html = createOption(name, abbreviation)
                stateID.innerHTML += html


                // const option = document.createElement('option');
                // option.innerHTML = Object.keys(state);
                // option.value = Object.values(state);
                // stateID.appendChild(option);
                // console.log(state)

            }
        }
    } catch (e) {
        console.error(e)
    }
})

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });
});